/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     DOUBLE = 258,
     INTEGER = 259,
     VALUE_CHAR = 260,
     VARIABLE = 261,
     FOR = 262,
     DO = 263,
     WHILE = 264,
     IF = 265,
     SWITCH = 266,
     BREAK = 267,
     CASE = 268,
     DEFAULT = 269,
     COLON = 270,
     INT = 271,
     FLOAT = 272,
     BOOL = 273,
     CHAR = 274,
     TRUE = 275,
     FALSE = 276,
     IFX = 277,
     ELSE = 278,
     NE = 279,
     EQ = 280,
     LE = 281,
     GE = 282,
     TOR = 283,
     TAND = 284,
     TNOT = 285,
     TXOR = 286,
     UMINUS = 287
   };
#endif
/* Tokens.  */
#define DOUBLE 258
#define INTEGER 259
#define VALUE_CHAR 260
#define VARIABLE 261
#define FOR 262
#define DO 263
#define WHILE 264
#define IF 265
#define SWITCH 266
#define BREAK 267
#define CASE 268
#define DEFAULT 269
#define COLON 270
#define INT 271
#define FLOAT 272
#define BOOL 273
#define CHAR 274
#define TRUE 275
#define FALSE 276
#define IFX 277
#define ELSE 278
#define NE 279
#define EQ 280
#define LE 281
#define GE 282
#define TOR 283
#define TAND 284
#define TNOT 285
#define TXOR 286
#define UMINUS 287



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 2058 of yacc.c  */
#line 20 "parser.y"

    float fValue;               /* float value */
    int iValue;                 /* integer value */
    char sIndex;                /* symbol table index */
    nodeType *nPtr;             /* node pointer */
    char identifier[32];


/* Line 2058 of yacc.c  */
#line 130 "y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
