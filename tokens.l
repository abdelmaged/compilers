%{
#include <string.h>
#include <stdlib.h>
#include "node.h"
#include "y.tab.hpp"
#define SAVE_TOKEN strcpy(yylval.identifier, yytext);
%}
%option yylineno
%%

[0-9]+[.][0-9]*   yylval.fValue = atof(yytext); return DOUBLE;
[0-9]+          yylval.iValue = atoi(yytext);   return INTEGER;
[-()<>=+*/;{}] return *yytext;
":"             return COLON;
"&"             return TAND;
"|"             return TOR;
"^"             return TXOR;
"~"             return TNOT;
">="            return GE;
"<="            return LE;
"=="            return EQ;
"!="            return NE;
"while"         return WHILE;
"do"            return DO;
"for"           return FOR;
"if"            return IF;
"else"          return ELSE;
"switch"        return SWITCH;
"break"         return BREAK;
"case"          return CASE;
"default"       return DEFAULT;
"int"           {return INT;}
"float"         {return FLOAT;}
"bool"          {return BOOL;}
"char"          {return CHAR;}
"true"			{return TRUE;}
"false"			{return FALSE;}
[\'][a-zA-Z0-9_][\'] { yylval.iValue=yytext[1]; return VALUE_CHAR;}
[a-zA-Z_][a-zA-Z0-9_]*       { 
                				SAVE_TOKEN;
                				return VARIABLE;
            				 }


[ \t\n]+        ;       /* ignore whitespace */

.		printf("Unknown token on line %d\n", yylineno); yyterminate();
%%


int yywrap (void){
    return 1;
}
