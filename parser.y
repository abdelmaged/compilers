%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <map>
#include "node.h"

extern int yylex();
extern int yylineno;
void yyerror(const char *s) { fprintf(stderr, "ERROR on line %d: %s\n", yylineno, s); exit(-1); }
extern int yyparse(void);
extern int ex(nodeType *p);

/* prototypes */
nodeType *opr(int oper, int nops, ...);
nodeType *id(char * identifier,int type);
nodeType *con(int ivalue,float fvalue,int Type);
void freeNode(nodeType *p);

std::map<int, int> sym;

%}



%union {
    float fValue;               /* float value */
    int iValue;                 /* integer value */
    char sIndex;                /* symbol table index */
    nodeType *nPtr;             /* node pointer */
    char identifier[32];
};
%token <fValue> DOUBLE
%token <iValue> INTEGER VALUE_CHAR
%token <identifier> VARIABLE 
%token FOR DO WHILE IF SWITCH BREAK CASE DEFAULT COLON INT FLOAT BOOL CHAR TRUE FALSE 
%nonassoc IFX
%nonassoc ELSE

%left GE LE EQ NE '>' '<'
%left '+' '-'
%left '*' '/'
%left TAND TOR
%left TXOR TNOT
%right '='
%nonassoc UMINUS

%type <nPtr> stmt expr stmt_list CASEBREAK SWITCHBLOCK DEFAULTBREAK CASEBREAKBlock

%%

program:
        function                { exit(0); }
        ;

function:
          function stmt         { ex($2); freeNode($2); }
        | /* NULL */
        ;

stmt:
          ';'                                           { $$ = opr(';', 2, NULL, NULL); }
        | expr ';'                                      { $$ = $1; }        
        | WHILE '(' expr ')' stmt                       { $$ = opr(WHILE, 2, $3, $5); }
        | DO stmt WHILE '(' expr ')' ';'                { $$ = opr(DO, 2, $5, $2); }
        | FOR'(' expr ';' expr ';' expr ')' stmt        { $$ = opr(FOR, 4, $3, $5 , $7 , $9); }
        | IF '(' expr ')' stmt %prec IFX                { $$ = opr(IF, 2, $3, $5); }
        | IF '(' expr ')' stmt ELSE stmt                { $$ = opr(IF, 3, $3, $5, $7); }
        | SWITCH '(' VARIABLE ')' '{' SWITCHBLOCK '}'   { $$ = opr(SWITCH, 2, $3, $6); }
        | '{' stmt_list '}'                             { $$ = $2; }
        | INT   VARIABLE ';'                            { $$ = id($2,INT); }
        | FLOAT VARIABLE ';'                            { $$ = id($2,FLOAT); }
        | BOOL  VARIABLE ';'                            { $$ = id($2,BOOL); }
        | CHAR  VARIABLE ';'                            { $$ = id($2,CHAR); }
        ;


SWITCHBLOCK:  CASEBREAKBlock                { $$ = $1; }
           |  CASEBREAKBlock DEFAULTBREAK   { $$ = opr(';', 2, $1, $2); }
           ;
CASEBREAKBlock:  CASEBREAK CASEBREAKBlock  { $$ = opr(';', 2, $1, $2); }
              | {}
              ;  
CASEBREAK: CASE INTEGER COLON stmt_list BREAK ';' { $$ = opr(CASE, 2,$2,$4); }
         | CASE VALUE_CHAR COLON stmt_list  BREAK ';' { $$ = opr(CASE, 2,$2,$4); }
         ;
DEFAULTBREAK:DEFAULT ':' stmt BREAK ';'  { $$ = opr(DEFAULT, 1, $3); }
            ;

stmt_list:
          stmt                  { $$ = $1; }
        | stmt_list stmt        { $$ = opr(';', 2, $1, $2); }
        ;
             
expr:
          INTEGER                 { $$ = con($1,0,INT); }
        | DOUBLE                  { $$ = con(0,$1,FLOAT); }
        | TRUE                    { $$ = con(1,0,BOOL); }
        | FALSE                   { $$ = con(0,0,BOOL); }
        | VALUE_CHAR              { $$ = con($1,0,CHAR); }
        | VARIABLE                { $$ = id($1,-1); }
        | VARIABLE '=' expr       { $$ = opr('=', 2, id($1,-1), $3); }
        | '-' expr %prec UMINUS   { $$ = opr(UMINUS, 1, $2); }
        | expr '+'  expr          { $$ = opr('+', 2, $1, $3); }
        | expr '-'  expr          { $$ = opr('-', 2, $1, $3); }
        | expr '*'  expr          { $$ = opr('*', 2, $1, $3); }
        | expr '/'  expr          { $$ = opr('/', 2, $1, $3); }
        | expr '<'  expr          { $$ = opr('<', 2, $1, $3); }
        | expr '>'  expr          { $$ = opr('>', 2, $1, $3); }
        | expr TAND expr          { $$ = opr('&', 2, $1, $3); }
        | expr TOR  expr          { $$ = opr('|', 2, $1, $3); }
        | expr TXOR expr          { $$ = opr('^', 2, $1, $3); }
        | expr TNOT expr          { $$ = opr('~', 2, $1, $3); }
        | expr GE   expr          { $$ = opr(GE, 2, $1, $3); }
        | expr LE   expr          { $$ = opr(LE, 2, $1, $3); }
        | expr NE   expr          { $$ = opr(NE, 2, $1, $3); }
        | expr EQ   expr          { $$ = opr(EQ, 2, $1, $3); }
        | '(' expr ')'            { $$ = $2; }
        ;

%%

#define SIZEOF_NODETYPE ((char *)&p->con - (char *)p)

nodeType *con(int ivalue,float fvalue,int Type) {

    nodeType *p;
    size_t nodeSize;

    /* allocate node */
    nodeSize = SIZEOF_NODETYPE + sizeof(conNodeType);
    if ((p = (nodeType *)malloc(nodeSize)) == NULL)
        yyerror("out of memory");

    /* copy information */
     p->id_Type = Type;
    if(Type==FLOAT){      
        p->con.fvalue = fvalue;
    }else{
        p->con.ivalue = ivalue;
    }  
    p->type = typeCon;
    return p;
}
nodeType *id(char * identifier,int type)  {
    nodeType *p;
    size_t nodeSize;

    /* allocate node */
    nodeSize = SIZEOF_NODETYPE + sizeof(idNodeType);
    if ((p = (nodeType *)malloc(nodeSize)) == NULL)
        yyerror("out of memory");

    /* copy information */
    p->id_Type = type;
    p->type = typeId;
    strcpy(p->id.Name, identifier);
    return p;
}

nodeType *opr(int oper, int nops, ...) {
    va_list ap;
    nodeType *p;
    size_t nodeSize;
    int i;

    /* allocate node */
    nodeSize = SIZEOF_NODETYPE + sizeof(oprNodeType) +
        (nops - 1) * sizeof(nodeType*);
    if ((p = (nodeType *)malloc(nodeSize)) == NULL)
        yyerror("out of memory");

    /* copy information */
    p->type = typeOpr;
    p->opr.oper = oper;
    p->opr.nops = nops;
    va_start(ap, nops);
    for (i = 0; i < nops; i++)
        p->opr.op[i] = va_arg(ap, nodeType*);
    va_end(ap);
    return p;
}

void freeNode(nodeType *p) {
    int i;

    if (!p) return;
    if (p->type == typeOpr) {
        for (i = 0; i < p->opr.nops; i++)
            freeNode(p->opr.op[i]);
    }
    free (p);
}

